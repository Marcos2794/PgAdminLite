﻿namespace pgAdmin
{
    partial class pgAdmingLite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Panel panel1;
            this.dgv_DataOutput = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.cbo_DataBase = new System.Windows.Forms.ComboBox();
            this.rtb_Query = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.trv_Server = new System.Windows.Forms.TreeView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.crearColumnaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eliminarTablaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip4 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createForeingKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Btn_Run = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pgAdminLite = new System.Windows.Forms.Label();
            panel1 = new System.Windows.Forms.Panel();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DataOutput)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.contextMenuStrip4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            panel1.AutoSize = true;
            panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            panel1.Controls.Add(this.dgv_DataOutput);
            panel1.Controls.Add(this.pictureBox2);
            panel1.Controls.Add(this.tabControl1);
            panel1.Controls.Add(this.label1);
            panel1.Controls.Add(this.trv_Server);
            panel1.Controls.Add(this.pictureBox1);
            panel1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            panel1.Location = new System.Drawing.Point(-2, 52);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(1252, 679);
            panel1.TabIndex = 4;
            // 
            // dgv_DataOutput
            // 
            this.dgv_DataOutput.AllowUserToDeleteRows = false;
            this.dgv_DataOutput.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_DataOutput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_DataOutput.GridColor = System.Drawing.SystemColors.ControlText;
            this.dgv_DataOutput.Location = new System.Drawing.Point(356, 436);
            this.dgv_DataOutput.Name = "dgv_DataOutput";
            this.dgv_DataOutput.ReadOnly = true;
            this.dgv_DataOutput.RowTemplate.Height = 24;
            this.dgv_DataOutput.Size = new System.Drawing.Size(893, 234);
            this.dgv_DataOutput.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(352, 29);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(897, 405);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.cbo_DataBase);
            this.tabPage3.Controls.Add(this.Btn_Run);
            this.tabPage3.Controls.Add(this.rtb_Query);
            this.tabPage3.ForeColor = System.Drawing.Color.Black;
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(889, 376);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Query";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Data Base";
            // 
            // cbo_DataBase
            // 
            this.cbo_DataBase.FormattingEnabled = true;
            this.cbo_DataBase.Location = new System.Drawing.Point(132, 11);
            this.cbo_DataBase.Name = "cbo_DataBase";
            this.cbo_DataBase.Size = new System.Drawing.Size(191, 24);
            this.cbo_DataBase.TabIndex = 2;
            // 
            // rtb_Query
            // 
            this.rtb_Query.Location = new System.Drawing.Point(-1, 42);
            this.rtb_Query.Name = "rtb_Query";
            this.rtb_Query.Size = new System.Drawing.Size(893, 337);
            this.rtb_Query.TabIndex = 0;
            this.rtb_Query.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(67, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Browser";
            // 
            // trv_Server
            // 
            this.trv_Server.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trv_Server.ForeColor = System.Drawing.Color.Black;
            this.trv_Server.Location = new System.Drawing.Point(3, 45);
            this.trv_Server.Name = "trv_Server";
            this.trv_Server.Size = new System.Drawing.Size(344, 625);
            this.trv_Server.TabIndex = 0;
            this.trv_Server.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.Crear_Click_Derecho);
            this.trv_Server.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.Seleccionar_Base_Click);
            this.trv_Server.Click += new System.EventHandler(this.inicia);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.WindowText;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.pgAdminLite);
            this.panel4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.panel4.Location = new System.Drawing.Point(-2, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1239, 54);
            this.panel4.TabIndex = 7;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTableToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(161, 28);
            this.contextMenuStrip1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CrearTabla_Click);
            // 
            // createTableToolStripMenuItem
            // 
            this.createTableToolStripMenuItem.Name = "createTableToolStripMenuItem";
            this.createTableToolStripMenuItem.Size = new System.Drawing.Size(160, 24);
            this.createTableToolStripMenuItem.Text = "Create Table";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearColumnaToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(173, 28);
            this.contextMenuStrip2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CrearColumna_Click);
            // 
            // crearColumnaToolStripMenuItem
            // 
            this.crearColumnaToolStripMenuItem.Name = "crearColumnaToolStripMenuItem";
            this.crearColumnaToolStripMenuItem.Size = new System.Drawing.Size(172, 24);
            this.crearColumnaToolStripMenuItem.Text = "CrearColumna";
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eliminarTablaToolStripMenuItem});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(267, 28);
            this.contextMenuStrip3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.EliminatTablas_Click);
            // 
            // eliminarTablaToolStripMenuItem
            // 
            this.eliminarTablaToolStripMenuItem.Name = "eliminarTablaToolStripMenuItem";
            this.eliminarTablaToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.eliminarTablaToolStripMenuItem.Text = "Eliminar/Truncar/Renombrar";
            this.eliminarTablaToolStripMenuItem.Click += new System.EventHandler(this.eliminarTablaToolStripMenuItem_Click);
            // 
            // contextMenuStrip4
            // 
            this.contextMenuStrip4.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createForeingKeyToolStripMenuItem});
            this.contextMenuStrip4.Name = "contextMenuStrip4";
            this.contextMenuStrip4.Size = new System.Drawing.Size(206, 28);
            this.contextMenuStrip4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Crear_Foreing_Key_Click);
            // 
            // createForeingKeyToolStripMenuItem
            // 
            this.createForeingKeyToolStripMenuItem.Name = "createForeingKeyToolStripMenuItem";
            this.createForeingKeyToolStripMenuItem.Size = new System.Drawing.Size(205, 24);
            this.createForeingKeyToolStripMenuItem.Text = "Create Foreing-Key";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = global::pgAdmin.Properties.Resources.if_Server_858733_opt_1_;
            this.pictureBox2.Location = new System.Drawing.Point(7, 48);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(21, 20);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // Btn_Run
            // 
            this.Btn_Run.BackgroundImage = global::pgAdmin.Properties.Resources.if_48_61512;
            this.Btn_Run.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Run.Location = new System.Drawing.Point(369, 3);
            this.Btn_Run.Name = "Btn_Run";
            this.Btn_Run.Size = new System.Drawing.Size(49, 37);
            this.Btn_Run.TabIndex = 1;
            this.Btn_Run.UseVisualStyleBackColor = true;
            this.Btn_Run.Click += new System.EventHandler(this.Btn_Run_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pgAdmin.Properties.Resources.if_evernote_279412_opt;
            this.pictureBox1.Location = new System.Drawing.Point(3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(42, 42);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pgAdminLite
            // 
            this.pgAdminLite.AutoSize = true;
            this.pgAdminLite.BackColor = System.Drawing.Color.Black;
            this.pgAdminLite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pgAdminLite.ForeColor = System.Drawing.SystemColors.Highlight;
            this.pgAdminLite.Image = global::pgAdmin.Properties.Resources.if_evernote_279412;
            this.pgAdminLite.Location = new System.Drawing.Point(65, 18);
            this.pgAdminLite.Name = "pgAdminLite";
            this.pgAdminLite.Size = new System.Drawing.Size(86, 17);
            this.pgAdminLite.TabIndex = 0;
            this.pgAdminLite.Text = "pgAdminLite";
            // 
            // pgAdmingLite
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1238, 724);
            this.Controls.Add(panel1);
            this.Controls.Add(this.panel4);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "pgAdmingLite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "pgAdminLite";
            this.Load += new System.EventHandler(this.pgAdmingLite_Load);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DataOutput)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip3.ResumeLayout(false);
            this.contextMenuStrip4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label pgAdminLite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TreeView trv_Server;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RichTextBox rtb_Query;
        private System.Windows.Forms.DataGridView dgv_DataOutput;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem createTableToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem crearColumnaToolStripMenuItem;
        private System.Windows.Forms.ComboBox cbo_DataBase;
        private System.Windows.Forms.Button Btn_Run;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem eliminarTablaToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip4;
        private System.Windows.Forms.ToolStripMenuItem createForeingKeyToolStripMenuItem;
    }
}

