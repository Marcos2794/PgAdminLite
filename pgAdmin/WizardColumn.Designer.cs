﻿namespace pgAdmin
{
    partial class WizardColumn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.eColumnasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgv_columns = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datatype = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.notnull = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.eColumnasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_columns)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_columns
            // 
            this.dgv_columns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_columns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.datatype,
            this.notnull});
            this.dgv_columns.Location = new System.Drawing.Point(13, 36);
            this.dgv_columns.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_columns.Name = "dgv_columns";
            this.dgv_columns.Size = new System.Drawing.Size(643, 198);
            this.dgv_columns.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(616, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(38, 22);
            this.button3.TabIndex = 4;
            this.button3.Text = "+";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::pgAdmin.Properties.Resources.if_filesave_6046;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(462, 242);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 48);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::pgAdmin.Properties.Resources.reset;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Location = new System.Drawing.Point(597, 242);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 48);
            this.button2.TabIndex = 3;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            this.name.Width = 200;
            // 
            // datatype
            // 
            this.datatype.HeaderText = "DataType";
            this.datatype.Items.AddRange(new object[] {
            "bigint",
            "bigserial",
            "bit ",
            "bit varying ",
            "boolean",
            "box ",
            "bytea ",
            "character varying ",
            "character ",
            "cidr ",
            "circle ",
            "date ",
            "double precision",
            "inet ",
            "integer",
            "interval",
            "line ",
            "lseg ",
            "macaddr ",
            "money ",
            "numeric ",
            "path ",
            "point ",
            "polygon ",
            "real",
            "smallint",
            "serial",
            "text ",
            "time ",
            "time ",
            "timestamp ",
            "timestamp ",
            "tsquery ",
            "tsvector ",
            "txid_snapshot ",
            "uuid ",
            "xml"});
            this.datatype.Name = "datatype";
            this.datatype.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.datatype.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.datatype.Width = 200;
            // 
            // notnull
            // 
            this.notnull.HeaderText = "Not Null?";
            this.notnull.Items.AddRange(new object[] {
            "",
            "Not Null"});
            this.notnull.Name = "notnull";
            this.notnull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.notnull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // WizardColumn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 303);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgv_columns);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "WizardColumn";
            this.Text = "WizardColumn";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Closing);
            this.Load += new System.EventHandler(this.WizardColumn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eColumnasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_columns)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource eColumnasBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipo;
        private System.Windows.Forms.DataGridView dgv_columns;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewComboBoxColumn datatype;
        private System.Windows.Forms.DataGridViewComboBoxColumn notnull;
    }
}