﻿namespace pgAdmin
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbo_Schema = new System.Windows.Forms.ComboBox();
            this.txt_Owner = new System.Windows.Forms.TextBox();
            this.txt_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.cbo_Tabla = new System.Windows.Forms.ComboBox();
            this.btn_Mascolumnas = new System.Windows.Forms.Button();
            this.dgv_Columns = new System.Windows.Forms.DataGridView();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pk = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.notnull = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_GuardarTablas = new System.Windows.Forms.Button();
            this.eColumnasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Columns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eColumnasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(565, 290);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbo_Schema);
            this.tabPage1.Controls.Add(this.txt_Owner);
            this.tabPage1.Controls.Add(this.txt_Name);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(557, 261);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // cbo_Schema
            // 
            this.cbo_Schema.FormattingEnabled = true;
            this.cbo_Schema.Items.AddRange(new object[] {
            "Public"});
            this.cbo_Schema.Location = new System.Drawing.Point(93, 103);
            this.cbo_Schema.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbo_Schema.Name = "cbo_Schema";
            this.cbo_Schema.Size = new System.Drawing.Size(220, 24);
            this.cbo_Schema.TabIndex = 9;
            this.cbo_Schema.SelectedIndexChanged += new System.EventHandler(this.cbo_Schema_SelectedIndexChanged);
            // 
            // txt_Owner
            // 
            this.txt_Owner.Location = new System.Drawing.Point(93, 64);
            this.txt_Owner.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_Owner.Name = "txt_Owner";
            this.txt_Owner.Size = new System.Drawing.Size(220, 22);
            this.txt_Owner.TabIndex = 8;
            // 
            // txt_Name
            // 
            this.txt_Name.Location = new System.Drawing.Point(93, 26);
            this.txt_Name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_Name.Name = "txt_Name";
            this.txt_Name.Size = new System.Drawing.Size(220, 22);
            this.txt_Name.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Schema";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Owner";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.cbo_Tabla);
            this.tabPage2.Controls.Add(this.btn_Mascolumnas);
            this.tabPage2.Controls.Add(this.dgv_Columns);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(557, 261);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Columns";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Seleccionar Base";
            // 
            // cbo_Tabla
            // 
            this.cbo_Tabla.FormattingEnabled = true;
            this.cbo_Tabla.Items.AddRange(new object[] {
            ""});
            this.cbo_Tabla.Location = new System.Drawing.Point(184, 7);
            this.cbo_Tabla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbo_Tabla.Name = "cbo_Tabla";
            this.cbo_Tabla.Size = new System.Drawing.Size(367, 24);
            this.cbo_Tabla.TabIndex = 4;
            // 
            // btn_Mascolumnas
            // 
            this.btn_Mascolumnas.Location = new System.Drawing.Point(11, 39);
            this.btn_Mascolumnas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Mascolumnas.Name = "btn_Mascolumnas";
            this.btn_Mascolumnas.Size = new System.Drawing.Size(35, 23);
            this.btn_Mascolumnas.TabIndex = 4;
            this.btn_Mascolumnas.Text = "+";
            this.btn_Mascolumnas.UseVisualStyleBackColor = true;
            this.btn_Mascolumnas.Click += new System.EventHandler(this.btn_Mascolumnas_Click);
            // 
            // dgv_Columns
            // 
            this.dgv_Columns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Columns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombre,
            this.tipo,
            this.pk,
            this.notnull});
            this.dgv_Columns.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgv_Columns.Location = new System.Drawing.Point(5, 37);
            this.dgv_Columns.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv_Columns.Name = "dgv_Columns";
            this.dgv_Columns.RowTemplate.Height = 24;
            this.dgv_Columns.Size = new System.Drawing.Size(545, 222);
            this.dgv_Columns.TabIndex = 0;
            this.dgv_Columns.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Columns_CellContentClick);
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre";
            this.nombre.Name = "nombre";
            this.nombre.Width = 200;
            // 
            // tipo
            // 
            this.tipo.HeaderText = "Tipo";
            this.tipo.Items.AddRange(new object[] {
            "bigint",
            "bigserial",
            "bit ",
            "bit varying ",
            "boolean",
            "box ",
            "bytea ",
            "character varying ",
            "character ",
            "cidr ",
            "circle ",
            "date ",
            "double precision",
            "inet ",
            "integer",
            "interval",
            "line ",
            "lseg ",
            "macaddr ",
            "money ",
            "numeric ",
            "path ",
            "point ",
            "polygon ",
            "real",
            "smallint",
            "serial",
            "text ",
            "time ",
            "time ",
            "timestamp ",
            "timestamp ",
            "tsquery ",
            "tsvector ",
            "txid_snapshot ",
            "uuid ",
            "xml"});
            this.tipo.Name = "tipo";
            // 
            // pk
            // 
            this.pk.HeaderText = "Indices";
            this.pk.Items.AddRange(new object[] {
            "Primary Key",
            "FOREIGN KEY"});
            this.pk.Name = "pk";
            this.pk.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.pk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // notnull
            // 
            this.notnull.HeaderText = "Not Null?";
            this.notnull.Items.AddRange(new object[] {
            "not null"});
            this.notnull.Name = "notnull";
            this.notnull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.notnull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::pgAdmin.Properties.Resources.reset;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Location = new System.Drawing.Point(317, 303);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(57, 51);
            this.button3.TabIndex = 3;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_GuardarTablas
            // 
            this.btn_GuardarTablas.BackgroundImage = global::pgAdmin.Properties.Resources.if_filesave_6046;
            this.btn_GuardarTablas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_GuardarTablas.Location = new System.Drawing.Point(182, 303);
            this.btn_GuardarTablas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_GuardarTablas.Name = "btn_GuardarTablas";
            this.btn_GuardarTablas.Size = new System.Drawing.Size(57, 51);
            this.btn_GuardarTablas.TabIndex = 1;
            this.btn_GuardarTablas.UseVisualStyleBackColor = true;
            this.btn_GuardarTablas.Click += new System.EventHandler(this.button1_Click);
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 375);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_GuardarTablas);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Wizard";
            this.Text = "Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Wizard_Closing);
            this.Load += new System.EventHandler(this.Wizard_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Columns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eColumnasBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt_Owner;
        private System.Windows.Forms.TextBox txt_Name;
        private System.Windows.Forms.Button btn_GuardarTablas;
        private System.Windows.Forms.DataGridView dgv_Columns;
        private System.Windows.Forms.BindingSource eColumnasBindingSource;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_Mascolumnas;
        private System.Windows.Forms.ComboBox cbo_Schema;
        private System.Windows.Forms.ComboBox cbo_Tabla;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipo;
        private System.Windows.Forms.DataGridViewComboBoxColumn pk;
        private System.Windows.Forms.DataGridViewComboBoxColumn notnull;
    }
}