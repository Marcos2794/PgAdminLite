﻿using pgAdminBOL;
using pgAdminENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pgAdmin
{
    public partial class WizardColumn : Form
    {
        private EColumnas crearcolumna;
        private ETablas creartabla;
        private CargarDatosBOL creaBOL;
        private EDatos basesENT;

        public WizardColumn()
        {
            InitializeComponent();

            dgv_columns.AllowUserToAddRows = false;
        }

        private void WizardColumn_Load(object sender, EventArgs e)
        {
            crearcolumna = new EColumnas();
            creartabla = new ETablas();
            creaBOL = new CargarDatosBOL();
            basesENT = new EDatos();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                string columna = "";


                EColumnas.Columnas = new List<EColumnas>();
                foreach (DataGridViewRow row in dgv_columns.Rows)
                {
                    crearcolumna = new EColumnas();
                    crearcolumna.Nombre = Convert.ToString(row.Cells["name"].Value);
                    crearcolumna.Tipo = Convert.ToString(row.Cells["datatype"].Value);
                    crearcolumna.NotNull = Convert.ToString(row.Cells["notnull"].Value);

                    EColumnas.Columnas.Add(crearcolumna);

                }
                foreach (var item in EColumnas.Columnas)
                {

                    columna += item.Nombre.ToString() + " " + item.Tipo.ToString() + " " + item.NotNull + " ,";
                    EAuxiliar.cantidadcolumnas++;
                }
                columna = columna.Remove(columna.Length - 1);
                

                creartabla.Name = EAuxiliar.tablas;

                MessageBox.Show(creaBOL.Crear2(creartabla, columna) ? "Guardado con éxito" : "Favor intente nuevamente");
                this.Hide();
                pgAdmingLite lt = new pgAdmingLite();
                lt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            dgv_columns.Rows.Clear();
        }

        private void frm_Closing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dgv_columns.Rows.Add("", "", "", "");
        }
    }
}

