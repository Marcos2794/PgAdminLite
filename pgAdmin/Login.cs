﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pgAdminENL;
using pgAdminBOL;

namespace pgAdmin
{
    public partial class Login : Form
    {
        private CargarDatosBOL ubo;
        public Login()
        {
            InitializeComponent();

        }

        private void Login_Load(object sender, EventArgs e)
        {
            ubo = new CargarDatosBOL();

        }
        private void IniciarSesion(object sender, EventArgs e)
        {
            EUsuario usu = new EUsuario
            {


                Password = txtPass.Text.Trim()

            };
            EAuxiliar.pass = txtPass.Text.Trim();



            usu = ubo.Login(usu);
            try
            {
                if (usu.Password != null)
                {


                    Hide();
                    pgAdmingLite pg = new pgAdmingLite();
                    pg.Show();

                    //LimpiarInterfaz();
                }

                else
                {
                    MessageBox.Show("Contraseña incorecto");

                }
            }
            catch
            {
                MessageBox.Show("Credenciales incorrectas");
            }

        }

    }
}
