﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using pgAdminBOL;
using pgAdminENL;

namespace pgAdmin
{
    public partial class pgAdmingLite : Form
    {
        private CargarDatosBOL dbBOL;
        private EDatos basesENT;
        private EDatosColumnas varENT;
        int posicion = 0;
        string[] Reservadas = EReservadas.Reservadas;

        public pgAdmingLite()
        {
            InitializeComponent();
            {
                resaltar();

                this.rtb_Query.SelectionStart = this.rtb_Query.Text.Length;

                this.rtb_Query.TextChanged += (ob, ev) =>
                {
                    posicion = rtb_Query.SelectionStart;
                    resaltar();
                };
            }
        }
        private void inicia(object sender, EventArgs e)
        {

            Login lo = new Login();

            if (EAuxiliar.cont == 0)
            {
                lo.Show();
                this.Hide();
                EAuxiliar.cont++;
            }


        }
        private void pgAdmingLite_Load(object sender, EventArgs e)
        {
            dbBOL = new CargarDatosBOL();
            basesENT = new EDatos();
            varENT = new EDatosColumnas();
            trv_Server.Nodes.Add("Servers");

        }
        /// <summary>
        /// Evento que controla el llenado del treeview
        /// </summary>
        private void Seleccionar_Base_Click(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                LlenarTreview();
            }
            else if (e.Node.Level == 2)
            {
                CargarBases();
            }
            else if (e.Node.Level == 3)
            {
                EAuxiliar.basedatos = e.Node.Text;
                LlenarBases();
            }

        }
        /// <summary>
        /// Metodo para resaltar palabras
        /// </summary>
        private void resaltar()
        {
            this.rtb_Query.Select(0, rtb_Query.Text.Length);
            this.rtb_Query.SelectionColor = Color.Black;
            this.rtb_Query.Select(posicion, 0);

            string[] texto = rtb_Query.Text.Trim().Split(' ');
            int inicio = 0;

            foreach (string x in texto)
            {
                foreach (string y in Reservadas)
                {
                    if (x.Length != 0)
                    {
                        if (x.Trim().Equals(y))
                        {
                            inicio = this.rtb_Query.Text.IndexOf(x, inicio);
                            this.rtb_Query.Select(inicio, x.Length);
                            rtb_Query.SelectionColor = Color.Red;
                            this.rtb_Query.Select(posicion, 0);
                            inicio = inicio + 1;
                        }
                    }
                }

            }
        }


        /// <summary>
        /// metodo para llenar trieview con bases de datos
        /// </summary>
        private void LlenarTreview()
        {
            trv_Server.SelectedNode.Nodes.Clear();
            trv_Server.SelectedNode.Nodes.Add("PostgreSQL 10");
            trv_Server.SelectedNode.LastNode.Nodes.Add("DataBases");
        }


        /// <summary>
        /// Metodo para cargar las bases de datos dentro DataBases
        /// </summary>
        private void CargarBases()
        {
            EAuxiliar.TodasLasTablas = new List<string>();

            trv_Server.SelectedNode.Nodes.Clear();
            foreach (var item in dbBOL.CargarBases())
            {
                trv_Server.SelectedNode.Nodes.Add(item.Nombre);
                EAuxiliar.TodasLasTablas.Add(item.Nombre);

            }
            cbo_DataBase.DataSource = EAuxiliar.TodasLasTablas;
        }

        /// <summary>
        /// Metodo para llenar las bases de datos con todos sus datos
        /// </summary>
        private void LlenarBases()
        {
            bool contador = false;
            trv_Server.SelectedNode.Nodes.Clear();
            trv_Server.SelectedNode.Nodes.Add("Tables");
            trv_Server.SelectedNode.Nodes.Add("Views");
            trv_Server.SelectedNode.Nodes.Add("Sequences");

            foreach (var item in dbBOL.LlenarTablas(EAuxiliar.basedatos))
            {

                trv_Server.SelectedNode.FirstNode.Nodes.Add(item.Nombre);
                trv_Server.SelectedNode.FirstNode.LastNode.Nodes.Add("Columns");
                trv_Server.SelectedNode.FirstNode.LastNode.Nodes.Add("Constraints");
                contador = false;
                foreach (var colum in dbBOL.LlenarColumnas(item.Nombre))
                {
                    trv_Server.SelectedNode.FirstNode.LastNode.FirstNode.Nodes.Add(colum.Variable);

                    if (contador == false)
                    {
                        foreach (var constraints in dbBOL.LlenarConstraints(item.Nombre))
                        {
                            contador = true;
                            trv_Server.SelectedNode.FirstNode.LastNode.LastNode.Nodes.Add(constraints.Constraint);
                        }
                    }
                }
            }
            foreach (var view in dbBOL.LlenarViews())
            {
                trv_Server.SelectedNode.FirstNode.NextNode.Nodes.Add(view.NombreVista);
            }
            foreach (var sequences in dbBOL.LlenarSequences())
            {
                trv_Server.SelectedNode.FirstNode.NextNode.NextNode.Nodes.Add(sequences.Sequences);
            }



        }

        /// <summary>
        /// Metodo que muestra el menu crear tabla
        /// </summary>
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            String CreateTable = trv_Server.SelectedNode.FullPath;

        }
        /// <summary>
        /// Metodo que muestra el form crear tabla 
        /// </summary>
        private void CrearTabla_Click(object sender, MouseEventArgs e)
        {
            this.Hide();
            Wizard mostrar = new Wizard();
            mostrar.Show(this);

        }
        /// <summary>
        /// Metodo que ejecuta el query
        /// </summary>
        private void Btn_Run_Click(object sender, EventArgs e)
        {

            dgv_DataOutput.Columns.Clear();
            try
            {

                EAuxiliar.datoscolumnas = new List<string>();

                foreach (var item in dbBOL.EjecutarQuery(rtb_Query.Text, cbo_DataBase.Text))
                {
                    EAuxiliar.datoscolumnas.Add(item.DatosQuery);
                };


                foreach (var item in EAuxiliar.nombrecolumnas)
                {
                    DataGridViewTextBoxColumn columna = new DataGridViewTextBoxColumn();
                    columna.HeaderText = item;
                    columna.Width = 200;
                    dgv_DataOutput.Columns.Add(columna);

                }
                int row = 0;
                int cell = 0;
                int cambio = EAuxiliar.nombrecolumnas.Count;
                int nuevacol = (EAuxiliar.datoscolumnas.Count / EAuxiliar.nombrecolumnas.Count) - 1;
                for (int i = 0; i < nuevacol; i++)
                {
                    dgv_DataOutput.Rows.Add();
                }
                foreach (var item in EAuxiliar.datoscolumnas)
                {
                    if (cambio == 0)
                    {
                        cambio = EAuxiliar.nombrecolumnas.Count;
                        row++;
                        cell = 0;
                    }

                    cambio = cambio - 1;
                    dgv_DataOutput.Rows[row].Cells[cell].Value = item;
                    dgv_DataOutput.ForeColor = Color.Black;
                    cell++;
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void CrearColumna_Click(object sender, MouseEventArgs e)
        {
            this.Hide();
            WizardColumn mostrar = new WizardColumn();
            mostrar.Show(this);

        }
        private void EliminatTablas_Click(object sender, MouseEventArgs e)
        {
            this.Hide();
            EliminarTruncarRenombrar el = new EliminarTruncarRenombrar();
            el.Show(this);

        }

        private void eliminarTablaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Metodo para crear con click derecho
        /// </summary>
        private void Crear_Click_Derecho(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)

            {

                trv_Server.SelectedNode = e.Node;


                if (e.Node != null && e.Node.Level == 3) //click on the root

                {
                    EAuxiliar.basedatos = e.Node.Text;
                    String CreateTable = trv_Server.SelectedNode.FullPath;

                    DirectoryInfo file = new DirectoryInfo(Path.Combine(CreateTable));

                    String atributos = file.Attributes.ToString();


                    if (!atributos.Contains(FileAttributes.Directory.ToString()))

                    {
                        this.contextMenuStrip1.Show(this.trv_Server, e.Location);

                    }

                }
                else if (e.Node != null && e.Node.Level == 5)
                {
                    string delColumn = trv_Server.SelectedNode.FullPath;
                    EAuxiliar.tablas = e.Node.Text;
                    EAuxiliar.basedatos = e.Node.Parent.Parent.Text;
                    DirectoryInfo file3 = new DirectoryInfo(Path.Combine(delColumn));

                    string atris = file3.Attributes.ToString();
                    if (!atris.Contains(FileAttributes.Directory.ToString()))
                    {
                        this.contextMenuStrip3.Show(this.trv_Server, e.Location);
                    }

                }
                else if (e.Node.Text != "Constraints" && e.Node.Level == 6)
                {
                    String CreateColumn = trv_Server.SelectedNode.FullPath;
                    EAuxiliar.tablas = e.Node.Parent.Text;
                    EAuxiliar.basedatos = e.Node.Parent.Parent.Parent.Text;
                    DirectoryInfo file2 = new DirectoryInfo(Path.Combine(CreateColumn));

                    String atri = file2.Attributes.ToString();
                    if (!atri.Contains(FileAttributes.Directory.ToString()))
                    {
                        this.contextMenuStrip2.Show(this.trv_Server, e.Location);
                    }
                }
                else if (e.Node.Text != "Columns" && e.Node.Level == 6)
                {
                    String CreateColumn = trv_Server.SelectedNode.FullPath;
                    EAuxiliar.tablas = e.Node.Parent.Text;
                    EAuxiliar.basedatos = e.Node.Parent.Parent.Parent.Text;
                    DirectoryInfo file1 = new DirectoryInfo(Path.Combine(CreateColumn));

                    String atri = file1.Attributes.ToString();
                    if (!atri.Contains(FileAttributes.Directory.ToString()))
                    {
                        this.contextMenuStrip4.Show(this.trv_Server, e.Location);
                    }
                }

            }
        }

        private void Crear_Foreing_Key_Click(object sender, MouseEventArgs e)
        {
            this.Hide();
            Frm_Create_Foreing_Key el = new Frm_Create_Foreing_Key();
            el.Show(this);
        }

    }
}


