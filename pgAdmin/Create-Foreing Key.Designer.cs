﻿namespace pgAdmin
{
    partial class Frm_Create_Foreing_Key
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txt_NombreRestriccion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_NuevaLocRef = new System.Windows.Forms.Button();
            this.dgv_CreateFK = new System.Windows.Forms.DataGridView();
            this.local = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenced = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbo_References = new System.Windows.Forms.ComboBox();
            this.cbo_Referencing = new System.Windows.Forms.ComboBox();
            this.cbo_Local = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_ResetCreateFK = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CreateFK)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(550, 300);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txt_NombreRestriccion);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(542, 271);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txt_NombreRestriccion
            // 
            this.txt_NombreRestriccion.Location = new System.Drawing.Point(161, 57);
            this.txt_NombreRestriccion.Name = "txt_NombreRestriccion";
            this.txt_NombreRestriccion.Size = new System.Drawing.Size(214, 22);
            this.txt_NombreRestriccion.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Local Columns";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_NuevaLocRef);
            this.tabPage2.Controls.Add(this.dgv_CreateFK);
            this.tabPage2.Controls.Add(this.cbo_References);
            this.tabPage2.Controls.Add(this.cbo_Referencing);
            this.tabPage2.Controls.Add(this.cbo_Local);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(542, 271);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Columns";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_NuevaLocRef
            // 
            this.btn_NuevaLocRef.Location = new System.Drawing.Point(496, 155);
            this.btn_NuevaLocRef.Name = "btn_NuevaLocRef";
            this.btn_NuevaLocRef.Size = new System.Drawing.Size(29, 21);
            this.btn_NuevaLocRef.TabIndex = 7;
            this.btn_NuevaLocRef.Text = "+";
            this.btn_NuevaLocRef.UseVisualStyleBackColor = true;
            this.btn_NuevaLocRef.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgv_CreateFK
            // 
            this.dgv_CreateFK.AllowUserToAddRows = false;
            this.dgv_CreateFK.AllowUserToDeleteRows = false;
            this.dgv_CreateFK.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_CreateFK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CreateFK.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.local,
            this.referenced});
            this.dgv_CreateFK.Location = new System.Drawing.Point(16, 152);
            this.dgv_CreateFK.Name = "dgv_CreateFK";
            this.dgv_CreateFK.ReadOnly = true;
            this.dgv_CreateFK.RowHeadersWidth = 10;
            this.dgv_CreateFK.RowTemplate.Height = 24;
            this.dgv_CreateFK.Size = new System.Drawing.Size(512, 98);
            this.dgv_CreateFK.TabIndex = 6;
            // 
            // local
            // 
            this.local.HeaderText = "Local";
            this.local.Name = "local";
            this.local.ReadOnly = true;
            this.local.Width = 250;
            // 
            // referenced
            // 
            this.referenced.HeaderText = "Referenced";
            this.referenced.Name = "referenced";
            this.referenced.ReadOnly = true;
            this.referenced.Width = 250;
            // 
            // cbo_References
            // 
            this.cbo_References.FormattingEnabled = true;
            this.cbo_References.Location = new System.Drawing.Point(166, 68);
            this.cbo_References.Name = "cbo_References";
            this.cbo_References.Size = new System.Drawing.Size(145, 24);
            this.cbo_References.TabIndex = 5;
            this.cbo_References.SelectedIndexChanged += new System.EventHandler(this.Seleccionar_Tabla_IndexChange);
            // 
            // cbo_Referencing
            // 
            this.cbo_Referencing.FormattingEnabled = true;
            this.cbo_Referencing.Location = new System.Drawing.Point(166, 109);
            this.cbo_Referencing.Name = "cbo_Referencing";
            this.cbo_Referencing.Size = new System.Drawing.Size(145, 24);
            this.cbo_Referencing.TabIndex = 4;
            // 
            // cbo_Local
            // 
            this.cbo_Local.FormattingEnabled = true;
            this.cbo_Local.Location = new System.Drawing.Point(166, 26);
            this.cbo_Local.Name = "cbo_Local";
            this.cbo_Local.Size = new System.Drawing.Size(145, 24);
            this.cbo_Local.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Referencing";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "References";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Local Columns";
            // 
            // btn_Save
            // 
            this.btn_Save.BackgroundImage = global::pgAdmin.Properties.Resources.if_filesave_6046;
            this.btn_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Save.Location = new System.Drawing.Point(325, 318);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(66, 55);
            this.btn_Save.TabIndex = 1;
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_ResetCreateFK
            // 
            this.btn_ResetCreateFK.BackgroundImage = global::pgAdmin.Properties.Resources.reset;
            this.btn_ResetCreateFK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_ResetCreateFK.Location = new System.Drawing.Point(429, 318);
            this.btn_ResetCreateFK.Name = "btn_ResetCreateFK";
            this.btn_ResetCreateFK.Size = new System.Drawing.Size(66, 55);
            this.btn_ResetCreateFK.TabIndex = 2;
            this.btn_ResetCreateFK.UseVisualStyleBackColor = true;
            this.btn_ResetCreateFK.Click += new System.EventHandler(this.btn_ResetCreateFK_Click);
            // 
            // Frm_Create_Foreing_Key
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 385);
            this.Controls.Add(this.btn_ResetCreateFK);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.tabControl1);
            this.Name = "Frm_Create_Foreing_Key";
            this.Text = "Create_Foreing_Key";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Closing);
            this.Load += new System.EventHandler(this.Frm_Create_Foreing_Key_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CreateFK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_CreateFK;
        private System.Windows.Forms.ComboBox cbo_References;
        private System.Windows.Forms.ComboBox cbo_Referencing;
        private System.Windows.Forms.ComboBox cbo_Local;
        private System.Windows.Forms.DataGridViewTextBoxColumn local;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenced;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_ResetCreateFK;
        private System.Windows.Forms.TextBox txt_NombreRestriccion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_NuevaLocRef;
    }
}