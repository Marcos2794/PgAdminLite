﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pgAdminBOL;
using pgAdminENL;

namespace pgAdmin
{

    public partial class EliminarTruncarRenombrar : Form
    {
        private EColumnas crearcolumna;
        private ETablas creartabla;
        private CargarDatosBOL creaBOL;
        private EDatos basesENT;
        public EliminarTruncarRenombrar()
        {
            InitializeComponent();
            carga();
            crearcolumna = new EColumnas();
            creartabla = new ETablas();
            creaBOL = new CargarDatosBOL();
            basesENT = new EDatos();

        }
        private void carga()
        {
            textBox1.Text = EAuxiliar.basedatos;
            textBox2.Text = EAuxiliar.tablas;

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {


        }



        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string columna = "(";


            try
            {

                columna = columna.Remove(columna.Length - 1);
                columna += ")";



                MessageBox.Show(creaBOL.elimina(EAuxiliar.basedatos, EAuxiliar.tablas)? "Eliminado correctamente":"No se pudo eliminar");
                
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EliminarTruncarRenombrar_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string columna = "(";
            EAuxiliar.nuevonom = textBox2.Text;

            try
            {


                columna = columna.Remove(columna.Length - 1);
                columna += ")";


                MessageBox.Show(creaBOL.renombra(EAuxiliar.basedatos, EAuxiliar.tablas) ? "Renombrado correctamente" : "No se pudo eliminar");

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string columna = "(";


            try
            {


                columna = columna.Remove(columna.Length - 1);
                columna += ")";



               MessageBox.Show( creaBOL.trunca(EAuxiliar.basedatos, EAuxiliar.tablas) ? "Truncado correctamente" : "No se pudo eliminar"); 

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Frm_Closing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }
    }
}
