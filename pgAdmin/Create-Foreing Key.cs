﻿using pgAdminBOL;
using pgAdminENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pgAdmin
{
    public partial class Frm_Create_Foreing_Key : Form
    {
        private CargarDatosBOL dbBOL;
        private EDatos LocalCol;
        private EColumnas RefeCol;
        string loc = "";
        string refer = "";
        string tablalocal = EAuxiliar.tablas;
        string nombrerestriccion = "";
        string tablareferencia = "";
        public Frm_Create_Foreing_Key()
        {
            InitializeComponent();

        }

        private void Frm_Create_Foreing_Key_Load(object sender, EventArgs e)
        {
            dbBOL = new CargarDatosBOL();
            LocalCol = new EDatos();
            RefeCol = new EColumnas();
            cargarcbo();
        }


        private void Frm_Closing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }
        /// <summary>
        /// Metodo para cargar las columnas y tablas en el cbo
        /// </summary>
        private void cargarcbo()
        {
            EAuxiliar.TodasLasTablas = new List<string>();
            foreach (var item in dbBOL.LlenarTablas(EAuxiliar.basedatos))
            {
                EAuxiliar.TodasLasTablas.Add(item.Nombre);
            }

            cbo_References.DataSource = EAuxiliar.TodasLasTablas;
            EAuxiliar.nombrecolumnas = new List<string>();

            foreach (var item in dbBOL.LlenarColumnas(tablalocal))
            {
                EAuxiliar.nombrecolumnas.Add(item.Variable);
            }
            cbo_Local.DataSource = EAuxiliar.nombrecolumnas;
        }
        /// <summary>
        /// Metodo para cargar el cbo_Referencing
        /// </summary>
        private void Cargarcbo_Referencing(string tabla)
        {
            cbo_Referencing.DataSource = null;
            EAuxiliar.nombrecolumnas = new List<string>();
            foreach (var item in dbBOL.LlenarColumnas(tabla))
            {
                EAuxiliar.nombrecolumnas.Add(item.Variable);
            }
            cbo_Referencing.DataSource = EAuxiliar.nombrecolumnas;
        }

        private void Seleccionar_Tabla_IndexChange(object sender, EventArgs e)
        {
            if (cbo_References.SelectedValue.ToString() != null)
            {
                EAuxiliar.tablas = cbo_References.SelectedValue.ToString();
                Cargarcbo_Referencing(EAuxiliar.tablas);
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {

            try
            {
                foreach (DataGridViewRow row in dgv_CreateFK.Rows)
                {
                    loc = Convert.ToString(row.Cells["local"].Value);
                    refer = Convert.ToString(row.Cells["referenced"].Value);
                }

                nombrerestriccion = txt_NombreRestriccion.Text;
                tablareferencia = cbo_References.Text;
                MessageBox.Show(dbBOL.CrearFK(nombrerestriccion, tablalocal, loc, refer,tablareferencia) ? "La llave se creo correctamente" : "Favor intente nuevamente");

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dgv_CreateFK.Rows.Add(cbo_Local.Text, cbo_Referencing.Text);
            btn_NuevaLocRef.Enabled = false;
        }

        private void btn_ResetCreateFK_Click(object sender, EventArgs e)
        {
            txt_NombreRestriccion.Text = "";
            dgv_CreateFK.Rows.Clear();
        }
    }
}
