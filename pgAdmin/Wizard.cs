﻿using pgAdminBOL;
using pgAdminENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pgAdmin
{
    public partial class Wizard : Form
    {
        private EColumnas crearcolumna;
        private ETablas creartabla;
        private CargarDatosBOL creaBOL;
        private EDatos basesENT;

        public Wizard()
        {
            InitializeComponent();
            dgv_Columns.AllowUserToAddRows = false;

        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            crearcolumna = new EColumnas();
            creartabla = new ETablas();
            creaBOL = new CargarDatosBOL();
            basesENT = new EDatos();
            LlenarCBO();
        }
        private void Frm_Wizard_Closing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }
        /// <summary>
        /// Metodo para llenar combobox
        /// </summary>
        private void LlenarCBO()
        {
            EAuxiliar.TodasLasTablas = new List<string>();
            foreach (var item in creaBOL.LlenarTablas(EAuxiliar.basedatos))
            {
                EAuxiliar.TodasLasTablas.Add(item.Nombre);
            }
            cbo_Tabla.DataSource = EAuxiliar.TodasLasTablas;
        }

        /// <summary>
        /// Metodo para crear tablas y columnas
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            string columna = "(";
            EAuxiliar.tablascbo = cbo_Tabla.Text;

            try
            {

                EColumnas.Columnas = new List<EColumnas>();
                foreach (DataGridViewRow row in dgv_Columns.Rows)
                {
                    crearcolumna = new EColumnas();
                    crearcolumna.Nombre = Convert.ToString(row.Cells["nombre"].Value);
                    crearcolumna.Tipo = Convert.ToString(row.Cells["tipo"].Value);
                    crearcolumna.PK = Convert.ToString(row.Cells["pk"].Value);
                    crearcolumna.NotNull = Convert.ToString(row.Cells["notnull"].Value);
                    EColumnas.Columnas.Add(crearcolumna);

                }
                foreach (var item in EColumnas.Columnas)
                {

                    columna += item.Nombre.ToString() + " " + item.Tipo.ToString() + " " + item.PK.ToString() + " " + item.NotNull + " ,";
                    EAuxiliar.cantidadcolumnas++;
                }
                columna = columna.Remove(columna.Length - 1);
                columna += ")";

                creartabla.Name = txt_Name.Text.Trim();
                creartabla.Owner = txt_Owner.Text.Trim();
                creartabla.Schema = cbo_Schema.Text;

                MessageBox.Show(creaBOL.Crear(creartabla, columna) ? "Guardado con éxito" : "Favor intente nuevamente");

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_Mascolumnas_Click(object sender, EventArgs e)
        {
            dgv_Columns.Rows.Add("", "", "", "", "");
        }
        /// <summary>
        /// Metodo para crear columnas
        /// </summary>



        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            txt_Name.Text = "";
            txt_Owner.Text = "";
            cbo_Schema.Text = "";
            dgv_Columns.Rows.Clear();
        }

        private void dgv_Columns_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cbo_Schema_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
