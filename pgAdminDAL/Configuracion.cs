﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Configuration;

namespace pgAdminDAL
{/// <summary>
/// Configuramos la conexion a postgresql
/// </summary>
    class Configuracion
    {

        public static string ConStr { get; } = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
    }
}
