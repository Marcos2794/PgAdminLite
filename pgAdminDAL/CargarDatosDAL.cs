﻿using Npgsql;
using pgAdminENL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;

namespace pgAdminDAL
{
    public class CargarDatosDAL
    {

        public List<EQuery> EjecutaQuery(string query, string database)
        {

            EAuxiliar.nombrecolumnas = new List<string>();

            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + database))
            {

                List<EQuery> datos = new List<EQuery>();
                con.Open();
                string sql = query;
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                for (int i = 0; i < reader.VisibleFieldCount; i++)
                {

                    EAuxiliar.nombrecolumnas.Add(reader.GetName(i));



                    while (reader.Read())
                    {
                        for (int j = 0; j < reader.VisibleFieldCount; j++)
                        {
                            string col = reader.GetName(j);
                            datos.Add(cargarquery(reader, col));
                        }

                    }
                }
                return datos;
            }
        }
        private EQuery cargarquery(NpgsqlDataReader reader, string col)
        {
            EQuery colum = new EQuery();
            colum.DatosQuery = reader[col].ToString();

            return colum;
        }

        /// <summary>
        /// Metodo para cargar las bases de datos
        /// </summary>
        public List<EDatos> CargarBases()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<EDatos> bases = new List<EDatos>();
                con.Open();
                string sql = @"SELECT datname FROM pg_database WHERE datistemplate ='False'";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    bases.Add(CargarBases(reader));
                }

                return bases;

            }
        }

        /// <summary>
        /// Metodo para capturar los datos del EDatos y devolverlos al metodo que lo solicite
        /// </summary>
        private EDatos CargarBases(NpgsqlDataReader reader)
        {
            EDatos bases = new EDatos();
            //TODO: No es necesario validar el id, es solo un ejemplo
            bases.Nombre = reader["datName"].ToString();

            return bases;
        }

        /// <summary>
        /// Metodo para logearse
        /// </summary>
        /// 
        public EUsuario Login(EUsuario usu)
        {
            {
                XmlDocument file;
                string Route = ("Datos.xml");
                file = new XmlDocument();
                file.Load(Route);

                XmlNodeList teacherlist = file.SelectNodes("Datos/Registrados");

                XmlNode teacher1;

                for (int i = 0; i < teacherlist.Count; i++)
                {
                    teacher1 = teacherlist.Item(i);


                    string Password = teacher1.SelectSingleNode("Password").InnerText;

                    if (Password.Equals(usu.Password))
                    {

                        usu.Password = teacher1.SelectSingleNode("Password").InnerText;

                        return usu;
                    }

                }
                return null;
            }
        }

        public bool renombra(string basedatos, string tablas)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {

                con.Open();
                string sql = @"ALTER table " + EAuxiliar.tablas + " RENAME TO " + EAuxiliar.nuevonom + ";";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();


                con.Close();
                return true;

            }
        }
        /// <summary>
        /// Metodo para generar FK
        /// </summary>
        public bool CrearFK(string nombrerestriccion, string tablalocal, string tablareferencia, string loc, string refer)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                con.Open();

                string sql = @"alter table " + tablalocal +
                               " add constraint " + nombrerestriccion +
                               " foreign key " + "(" + loc + ")" +
                               " references " + tablareferencia + "(" + refer + ")";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                con.Close();
                return true;
            }
        }

        public bool trunca(string basedatos, string tablas)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {

                con.Open();
                string sql = @"TRUNCATE TABLE " + EAuxiliar.tablas + " CASCADE;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();


                con.Close();
                return true;

            }
        }

        /// <summary>
        /// Metodo para llenar las tablas con las columnas en cada base de datos
        /// </summary>
        public List<EDatos> LlenarTablas(string basededatos)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                List<EDatos> columnas = new List<EDatos>();
                con.Open();
                string sql = @"SELECT tablename FROM pg_catalog.pg_tables
                                WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    columnas.Add(BuscarColumnas(reader));
                }
                con.Close();
                return columnas;

            }
        }

        /// <summary>
        /// Metodo para capturar el nombre de la columna retornalo y almazenarlo en una lista
        /// </summary>
        private EDatos BuscarColumnas(NpgsqlDataReader reader)
        {
            EDatos bases = new EDatos();
            //TODO: No es necesario validar el id, es solo un ejemplo
            bases.Nombre = reader["tablename"].ToString();

            return bases;
        }
        /// <summary>
        /// Metodo para buscar todas las vistas de una base de datos
        /// </summary>
        public List<EVistas> LlenarVista()
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                List<EVistas> vistas = new List<EVistas>();
                con.Open();
                string sql = @"SELECT table_name 
                               FROM Information_Schema.Tables 
                               WHERE table_type = 'VIEW' And table_schema = 'public'";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    vistas.Add(BuscarVistas(reader));
                }
                con.Close();
                return vistas;

            }
        }

        /// <summary>
        /// Metodo para capturar el nombre de la vista retornarlo y almacenarlo en una lista
        /// </summary>
        private EVistas BuscarVistas(NpgsqlDataReader reader)
        {
            EVistas nombre = new EVistas();
            //TODO: No es necesario validar el id, es solo un ejemplo
            nombre.NombreVista = reader["table_name"].ToString();

            return nombre;
        }
        /// <summary>
        /// Metodo para llenar los sequences de cada base de datos
        /// </summary>
        public List<ESequences> LlenarSequence()
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                List<ESequences> sequences = new List<ESequences>();
                con.Open();
                string sql = @"SELECT sequence_name FROM information_schema.sequences ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    sequences.Add(BuscarSequences(reader));
                }
                con.Close();
                return sequences;

            }
        }


        private ESequences BuscarSequences(NpgsqlDataReader reader)
        {
            ESequences nombre = new ESequences();
            //TODO: No es necesario validar el id, es solo un ejemplo
            nombre.Sequences = reader["sequence_name"].ToString();

            return nombre;
        }

        /// <summary>
        /// Metodo para llenar las columnas con las variables de cada una en cada base de datos
        /// </summary>
        public List<EDatosColumnas> LlenarColumnas(string columna)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                List<EDatosColumnas> variables = new List<EDatosColumnas>();
                con.Open();
                string sql = @"SELECT column_name FROM information_schema.columns 
                                WHERE table_schema = 'public' AND table_name   ='" + columna + "'";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    variables.Add(BuscarVariables(reader));
                }

                return variables;

            }
        }

        /// <summary>
        /// Metodo para capturar el nombre dela variable  retornalo y almazenarlo en una lista
        /// </summary>
        private EDatosColumnas BuscarVariables(NpgsqlDataReader reader)
        {
            EDatosColumnas nombre = new EDatosColumnas();
            //TODO: No es necesario validar el id, es solo un ejemplo
            nombre.Variable = reader["column_name"].ToString();

            return nombre;
        }
        /// <summary>
        /// Metodo para llenar los constrains
        /// </summary>
        public List<EConstraint> LlenarConstraint(string columna)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                List<EConstraint> variables = new List<EConstraint>();
                con.Open();
                string sql = @"SELECT constraint_name 
                               FROM information_schema.table_constraints 
                               WHERE table_schema = 'public' AND constraint_type !='CHECK' 
                               AND table_name='" + columna + "'";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    variables.Add(BuscarConstraint(reader));
                }

                return variables;

            }
        }
        /// <summary>
        /// Metodo para buscar los constraint de cada columna
        /// </summary>
        private EConstraint BuscarConstraint(NpgsqlDataReader reader)
        {
            EConstraint nombre = new EConstraint();
            //TODO: No es necesario validar el id, es solo un ejemplo
            nombre.Constraint = reader["constraint_name"].ToString();

            return nombre;
        }

        /// <summary>
        /// Metodo para crear solo columnas
        /// </summary>
        public bool crearcolumnas(string columna)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                con.Open();

                string sql = @"Alter Table " + EAuxiliar.tablas + " Add " + columna + "";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                con.Close();
                return true;
            }
        }

        /// <summary>
        /// Metodo para crear solo tablas
        /// </summary>
        public bool crearTabla(ETablas tabla)
        {

            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                con.Open();
                string sql = @"Create Table " + tabla.Name + "()";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                return true;
            }

        }

        /// <summary>
        /// Metodo para crera tablas y columnas
        /// </summary>
        public bool creartablacolumna(ETablas Tablas, string columna)
        {

            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {
                con.Open();
                string sql = @"Create Table " + Tablas.Name + "" + columna + "";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                return true;
            }

        }


        public bool elimina(string basedatos, string tablas)
        {
            using (NpgsqlConnection con = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres;Password=" + EAuxiliar.pass + ";Database=" + EAuxiliar.basedatos))
            {

                con.Open();
                string sql = @"DROP TABLE IF EXISTS " + EAuxiliar.tablas + ";";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();


                con.Close();
                return true;

            }
        }



    }
}
