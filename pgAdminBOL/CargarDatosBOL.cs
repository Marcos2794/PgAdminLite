﻿
using pgAdminDAL;
using pgAdminENL;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace pgAdminBOL
{
    public class CargarDatosBOL
    {
        CargarDatosDAL basesDAL;
        private CargarDatosDAL uda;

        public CargarDatosBOL()
        {
            basesDAL = new CargarDatosDAL();
            uda = new CargarDatosDAL();
            basesDAL = new CargarDatosDAL();
            uda = new CargarDatosDAL();
        }
        /// <summary>
        /// Metodo para cargar las bases de datos
        /// </summary>
        public List<EDatos> CargarBases()
        {
            return basesDAL.CargarBases();
        }
        /// <summary>
        /// Metodo para cargar las tablas
        /// </summary>
        public List<EDatos> LlenarTablas(string basedatos)
        {
            return basesDAL.LlenarTablas(basedatos);
        }

        public EUsuario Login(EUsuario usu)
        {

            if (String.IsNullOrEmpty(usu.Password))
            {

                return null;
            }


            return uda.Login(usu);
        }

        /// <summary>
        /// Metodo para cargar las columnas
        /// </summary>
        public List<EDatosColumnas> LlenarColumnas(string tablas)
        {

            return basesDAL.LlenarColumnas(tablas);
        }

        public bool elimina(string basedatos, string tablas)
        {
            if (String.IsNullOrEmpty(basedatos) ||
                   String.IsNullOrEmpty(tablas))
            {
                throw new Exception("Faltan iformacion para ejecutar la operacion");
            }
            return basesDAL.elimina(basedatos, tablas);
        }

        /// <summary>
        /// Metodo para crear tablas y columnas graficamente
        /// </summary>
        public bool Crear(ETablas tabla, string columna)
        {
            if (tabla.Name != "" && columna != ")")
            {
                if (String.IsNullOrEmpty(tabla.Name) ||
                    String.IsNullOrEmpty(tabla.Owner) ||
                    String.IsNullOrEmpty(tabla.Schema))
                {
                    throw new Exception("Faltan datos generales para crear la tabla");
                }
                return basesDAL.creartablacolumna(tabla, columna);
            }
            else if (tabla.Name != "" && columna.Equals(")"))
            {
                if (String.IsNullOrEmpty(tabla.Name) ||
                      String.IsNullOrEmpty(tabla.Owner) ||
                      String.IsNullOrEmpty(tabla.Schema))
                {
                    throw new Exception("Faltan datos generales para crear la tabla");
                }
                return basesDAL.crearTabla(tabla);
            }
            else if (columna != ")" & tabla.Name.Equals(""))
            {
                if (String.IsNullOrEmpty(EAuxiliar.tablascbo))

                {
                    throw new Exception("Seleccione la tabla donde desea ingresar la nueva columna");
                }
                if (EAuxiliar.cantidadcolumnas > 1)
                {
                    throw new Exception("Solo puede agregar una columna a la vez");
                }
                columna = columna.Replace("(", "");
                columna = columna.Replace(")", "");

                return basesDAL.crearcolumnas(columna);
            }

            return false;
        }

        public bool trunca(string basedatos, string tablas)
        {
            if (String.IsNullOrEmpty(basedatos) ||
                  String.IsNullOrEmpty(tablas))
            {
                throw new Exception("Faltan iformacion para ejecutar la operacion");
            }
            return basesDAL.trunca(basedatos, tablas);
        }
        /// <summary>
        /// Metodo para crear FK
        /// </summary>
        public bool CrearFK(string nombrerestriccion, string tablalocal, string loc, string refer, string tablareferencia)
        {

            if (String.IsNullOrEmpty(nombrerestriccion) ||
                String.IsNullOrEmpty(tablareferencia) ||
                String.IsNullOrEmpty(tablalocal) ||
                String.IsNullOrEmpty(loc) ||
                String.IsNullOrEmpty(refer))
            {
                throw new Exception("Faltan datos generales para crear la tabla");
            }
            return basesDAL.CrearFK(nombrerestriccion, tablalocal, tablareferencia, loc, refer);
        }

        public bool renombra(string basedatos, string tablas)
        {
            if (String.IsNullOrEmpty(basedatos) ||
                  String.IsNullOrEmpty(tablas))
            {
                throw new Exception("Faltan iformacion para ejecutar la operacion");
            }
            return basesDAL.renombra(basedatos, tablas);
        }

        public bool Crear2(ETablas tabla, string columna)
        {

                return basesDAL.crearcolumnas(columna);


            return false;
        }

        public List<EVistas> LlenarViews()
        {
            return basesDAL.LlenarVista();
        }

        public List<ESequences> LlenarSequences()
        {
            return basesDAL.LlenarSequence();
        }

        public List<EConstraint> LlenarConstraints(string columna)
        {
            return basesDAL.LlenarConstraint(columna);
        }

        public List<EQuery> EjecutarQuery(string query, string database)
        {

            if (String.IsNullOrEmpty(database))
            {
                throw new Exception("Seleccione una base de datos");
            }
            if (String.IsNullOrEmpty(query))
            {
                throw new Exception();
            }
            return basesDAL.EjecutaQuery(query, database);
        }
    }
}
