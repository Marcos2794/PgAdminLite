﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pgAdminENL
{
    public class EColumnas
    {
        public static List<EColumnas> Columnas { get; set; }

        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string Tamaño { get; set; }
        public string NotNull { get; set; }
        public string PK { get; set; }
    }
}
