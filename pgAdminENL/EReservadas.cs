﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pgAdminENL
{
    public class EReservadas
    {
        public static string[] Reservadas = new string[] { "ABORT","ABSOLUTE","ACCESS","ACTION","ADD","AGGREGATE","ALTER","ANALYSE",
        "ANALYZE","ANY","ARRAY","ASSERTION","ASSIGNMENT","AT","AUTHORIZATION","BACKWARD","BIGINT","BINARY","BIT","BOOLEAN",
        "BOTH","CACHE","CALLED","CAST","CHAIN","CHAR","CHARACTER","CHARACTERISTICS","CHECKPOINT","CLASS","CLOSE","CLUSTER",
        "COALESCE","COLUMN","COMMENT","COMMITTED","CONSTRAINTS","CONVERSION","CONVERT","COPY","CREATE","CREATEDB","CREATEUSER",
        "CURRENT_DATE","CURRENT_TIME","CURRENT_TIMESTAMP","CURRENT_USER","CURSOR","CYCLE","DAY","DEALLOCATE","DEC","DECIMAL",
        "DECLARE","DEFAULTS","DEFERRABLE","DEFERRED","DEFINER","DELIMITER","DELIMITERS","DO","DOMAIN","DOUBLE","EACH",
        "ENCODING","ENCRYPTED","ESCAPE","EXCEPT","EXCLUDING","EXCLUSIVE","EXECUTE","EXISTS","EXTERNAL","EXTRACT","FALSE",
        "FETCH","FIRST","FLOAT","FORCE","FORWARD","FROM","FREEZE","FUNCTION","GLOBAL","GRANT","HANDLER","HOLD","HOUR","ILIKE",
        "IMMEDIATE","IMMUTABLE","IMPLICIT","INCLUDING","INCREMENT","INHERITS","INITIALLY","INOUT","INPUT","INSENSITIVE",
        "INSTEAD","INT","INTERSECT","INTERVAL","INVOKER","INSERT","ISNULL","ISOLATION","LANCOMPILER","LANGUAGE","LAST","LEADING",
        "LEVEL","LISTEN","LOAD","LOCAL","LOCALTIME","LOCALTIMESTAMP","LOCATION","LOCK","MAXVALUE","MINUTE","MINVALUE","MODE",
        "MONTH","MOVE","NAMES","NATIONAL","NCHAR","NEW","NEXT","NO","NOCREATEDB","NOCREATEUSER","NONE","NOTHING","NOTIFY",
        "NOTNULL","NULLIF","NUMERIC","OF","OFF","OIDS","OLD","ONLY","OPERATOR","OPTION","OUT","OVERLAPS","OVERLAY","OWNER",
        "PARTIAL","PASSWORD","PATH","PENDANT","PLACING","POSITION","PRECISION","PREPARE","PRESERVE","PRIOR","PRIVILEGES",
        "PROCEDURAL","PROCEDURE","READ","REAL","RECHECK","REINDEX","RELATIVE","RENAME","RESET","RESTART","RETURNS","REVOKE",
        "ROWS","RULE","SCHEMA","SCROLL","SECOND","SECURITY","SELECT","SEQUENCE","SERIALIZABLE","SESSION","SESSION_USER","SETOF","SHARE",
        "SHOW","SIMPLE","SMALLINT","SOME","STABLE","START","STATEMENT","STATISTICS","STDIN","STDOUT","STORAGE","STRICT",
        "SUBSTRING","SYSID","TEMP","TEMPLATE","TIME","TIMESTAMP","TOAST","TRAILING","TREAT","TRIGGER","TRIM","TRUE",
        "TRUNCATE","TRUSTED","TYPE","UPDATE","UNENCRYPTED","UNKNOWN","UNLISTEN","UNTIL","USAGE","USER","VACUUM","VALID","VALIDATOR",
        "VARCHAR","VARYING","VERBOSE","VERSION","VIEW","VOLATILE","WITH","WITHOUT","WORK","WRITE","YEAR","ZONE","abort","absolute","access","action",
        "add","aggregate","alter","analyse","analyze","any","array","assertion","assignment","at","autorization","backward","bigint","binary","bit",
        "boolean","both","cache","called","cast","chain","char","character","characteristics","checkpoint","class","close","cluster","coalesce","column",
        "comment","commited","constraints","conversion","convert","copy","create","createdb","createuser","current_date","current_time","current_timestamp",
        "current_user","cursor","cycle","day","deallocate","dec","decimal","declare","defaults","deferrable","deferred","definer","delimiter","delimiters",
        "do","domain","double","each","encoding","encrypted","escape","except","excluding","exclusive","excecute","exists","external","extract","false",
        "fetch","first","float","force","forward","from","freeze","function","global","grant","handler","hold","hour","ilike","immediate","immutable",
        "implicit","including","increment","inherits","initially","inout","input","insesitive","instead","int","intersect","interval","invoker","insert",
        "isnull","isolation","lancompiler","language","last","leading","level","listen","load","local","localtime","localtimestamp","location","lock",
        "maxvalue","minute","minvalue","mode","month","move","names","national","nchar","new","next","no","nocreatedb","nocreateuser","none","nothing",
        "notify","notnull","nullif","numeric","of","off","oids","old","only","operator","option","out","overlaps","overlay","owner","partial","password",
        "path","pendant","placing","position","precision","prepare","preserve","prior","privileges","procedural","procedure","read","real","recheck","reindex",
        "relative","rename","reset","restart","returns","revoke","rows","rule","schema","scroll","second","security","select","sequence","serializable",
        "session","session_user","setof","share","show","simple","smallint","some","stable","start","statement","statistics","stdin","stdout","storage",
        "strict","substring","sysid","temp","template","time","timestamp","toast","trailing","treat","trigger","trim","true","truncate","trsuted","type",
        "update","unencrypted","unknown","unlisten","until","usage","user","vacuum","valid","validator","varchar","varying","verbose","version","view",
        "volatile","with","without","work","write","year","zone"};
    }
}
